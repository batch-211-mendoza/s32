const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {
	if (request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from database");
	}

	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	}
});

server.listen(port);

console.log(`Server running at localhost:${port}.`);

/*
	1. Open gitbash on d1 folder
	2. Enter "npm install -g nodemon"
	3. Enter "nodemon route.js"
*/